import cv2
import numpy as np

minimo = [0,0];
maximo = [0,0];

filename = 'croped1.jpg'
img = cv2.imread(filename)

img = cv2.GaussianBlur(img,(9,9),0)

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

gray = np.float32(gray)

dst = cv2.cornerHarris(gray,2,3,0.04)

#result is dilated for marking the corners, not important
dst = cv2.dilate(dst,None)

# Threshold for an optimal value, it may vary depending on the image.
img[dst>0.01*dst.max()]=[0,0,255]

cv2.imshow('dst',img)

minimo[0] = len(img[0])
minimo[1] = len(img)

for i in range(len(img)):
    for j in range(len(img[i])):
        if (img[i][j][0]==0 and img[i][j][1]==0 and img[i][j][2]==255):            
            if i<=minimo[0]:
                minimo[0] = i
            if j<=minimo[1]:
                minimo[1] = j
            if i>=maximo[0]:
                maximo[0] = i
            if j>=maximo[1]:
                maximo[1] = j                

cv2.imshow('dst',img[minimo[0]:maximo[0],minimo[1]:maximo[1]])
if cv2.waitKey(0) & 0xff == 27:
    cv2.destroyAllWindows()
