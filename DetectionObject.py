from PIL import Image
from matplotlib import pyplot
import numpy as np
import commands
import cv2
import os

cont = 0

def saveOCR2(image):
    #Loading temp fie
    img = cv2.imread(image)

    #Saving global
    cv2.imwrite('toOCR.jpg',img)    

    commands.getoutput('tesseract toOCR.jpg '+image+' -psm 6')
    
def fitPlate(filename):
    
    minimo = [0,0];
    maximo = [0,0];

    img = cv2.imread(filename)

    copyImg = img
    
    img = cv2.GaussianBlur(img,(9,9),0)

    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    gray = np.float32(gray)

    dst = cv2.cornerHarris(gray,2,3,0.04)

    #result is dilated for marking the corners, not important
    dst = cv2.dilate(dst,None)

    # Threshold for an optimal value, it may vary depending on the image.
    img[dst>0.01*dst.max()]=[0,0,255]

    #cv2.imshow('dst',img)

    minimo[0] = len(img[0])
    minimo[1] = len(img)

    for i in range(len(img)):
        for j in range(len(img[i])):
            if (img[i][j][0]==0 and img[i][j][1]==0 and img[i][j][2]==255):            
                if i<=minimo[0]:
                    minimo[0] = i
                if j<=minimo[1]:
                    minimo[1] = j
                if i>=maximo[0]:
                    maximo[0] = i
                if j>=maximo[1]:
                    maximo[1] = j                

    return copyImg[minimo[0]:maximo[0],minimo[1]:maximo[1]]

    
def saveOCR (image,path,type):

    output = ""

    #Saving temp file
    cv2.imwrite('temp.jpg',image)

    #Loading temp fie
    img = cv2.imread("temp.jpg")

    #Smothing image
    img = cv2.GaussianBlur(img,(9,9),0)

    #To Grayscale
    img =  cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    #Setting global thresholding
    #ret2,th2 = cv2.threshold(img,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    th2 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
    #ret2,th2 = cv2.threshold(img,127,255,cv2.THRESH_BINARY)

    #Saving global
    cv2.imwrite('global.jpg',th2)

    #Open image
    usrimg = Image.open("global.jpg")
    
    # Converting the image to greyscale.
    captcha = usrimg.convert('1')
    
    # Saving the image as tesseract can read.
    captcha.save('temp.bmp', dpi=(600,600))
    
    # Invoking tesseract from python to extract characters

    if (type==0):
        commands.getoutput('tesseract temp.jpg '+path+' -psm 5 digits')        
    else:
        commands.getoutput('tesseract temp.jpg '+path)

    # Saving the output generated as a string
    with open(path+'.txt', 'r') as data:
        for i in data:
                output+=i.strip()

    if(type==0):
        output = output[::-1]
        
    return output

def detectLiscencePlate(path):

    #Opening image

    img = cv2.imread(path)    
    
    #Print Image Sizes
    h, w, e = img.shape
    #print h,w


    #Spliting Image
    left = img[0:h,0:((w/7)*3)]
    right = img[0:h,((w/7)*3):w]

    #Send left and right image to OCR  
    #saveOCR returns a String
    #saveOCR (image to recognize, file to save temporaly, 0 if is a numebers and 1 if is letters )
    
    left = saveOCR(left,"data",1)
    right = saveOCR(right,"data2",0)

    output= left+"-"+right

    return output

def cropPlate(name,outPath):
    global cont
    possiblesPlate=[]
    arqCasc2 = 'licence_plate_2.xml'
    plateCascade1 = cv2.CascadeClassifier(arqCasc2) #classificador para placa    
        
    #Open image
    imagem = cv2.imread(name)

    #To Grayscale
    #imagem =  cv2.cvtColor(imagem, cv2.COLOR_BGR2GRAY)
    
    plate = plateCascade1.detectMultiScale(
                imagem,
                minNeighbors=0,
                minSize=(3,1),
                maxSize=(600,200)
            )    
    
    for (x, y, w, h) in plate:
        
        img = imagem[y:y+h,x:x+w]
      
        cv2.imwrite(outPath+"croped"+str(cont)+".jpg",img)

        img = fitPlate(outPath+"croped"+str(cont)+".jpg")

        cv2.imwrite(outPath+"croped"+str(cont)+".jpg",img)
        
        possiblesPlate.append(outPath+"croped"+str(cont)+".jpg")                
        cont+=1
    
    return possiblesPlate

def detect(possiblePlates):
    for i in possiblePlates:
        print detectLiscencePlate(i)                

def generateCroped(path):
    for nome in os.listdir(path):
        print nome
        cropPlate(path+'/'+nome)
    

#path = raw_input("Input the liscense plate path: ")
#detect(cropPlate(path,'CROPED/'))
#generateCroped('BASE')
#for nome in os.listdir('figuras/Capture'):
#        print nome
#        cropPlate('figuras/Capture/'+nome,'BASE/')
cv2.imwrite('imagem.jpg',fitPlate('imagem.jpg'))
